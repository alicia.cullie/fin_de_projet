#include <Arduino.h>
#define MOTDROIT_P 11 // p : positif
#define MOTDROIT_N 12 // n: negatif
#define MOTGAUCH_N 5
#define MOTGAUCH_P 6

#define ZONE_MORTE 80
int Val1 = 0;
int Val2=0;
int CapteurD = 8;
int CapteurG = 7;


void setup() {

Serial.begin(9600);
pinMode(MOTDROIT_N,OUTPUT);
pinMode(MOTDROIT_P,OUTPUT);
pinMode(MOTGAUCH_N,OUTPUT);
pinMode(MOTGAUCH_P,OUTPUT);
pinMode(CapteurD, INPUT);
pinMode(CapteurG, INPUT);

analogWrite(MOTDROIT_N, LOW );
analogWrite(MOTDROIT_P, LOW );
analogWrite(MOTGAUCH_N, LOW );
analogWrite(MOTGAUCH_P, LOW );

  // put your setup code here, to run once:
}


// fonction de génération de la commande du moteur1

void driveMotDroit(int sens)
{
  
  if (sens< -255) 
  {
    sens= -255 ;
  }
  
  if (sens> 255)
  {
    sens = 255 ;
  }
  if ( (sens< ZONE_MORTE)  && (sens < -ZONE_MORTE))
  {
   sens=0;

   }
 
 if (sens>=0)
 {
   analogWrite(MOTDROIT_N,sens);
   analogWrite(MOTDROIT_P, 0);
 }
 else if ( sens<0)
 {
   analogWrite(MOTDROIT_N, 0);
   analogWrite(MOTDROIT_P, -sens);

 }
}


 void driveMotGauche(int sens2)
{

  if (sens2< -255) 
  {
    sens2= -255 ;
  }
  
  if (sens2 > 255)
  {
    sens2 = 255 ;
  }
  if ( (sens2< ZONE_MORTE)  && (sens2 < -ZONE_MORTE))
  {
   sens2=0;

   }
 
  if (sens2>0)
  {
  analogWrite(MOTGAUCH_N,sens2);
  analogWrite(MOTGAUCH_P, 0);
  }
  else if ( sens2 <0)
  {
  analogWrite(MOTGAUCH_N, 0);
  analogWrite(MOTGAUCH_P, -sens2);

  }else
  {
  analogWrite(MOTGAUCH_N, LOW);
  analogWrite(MOTGAUCH_P, LOW);
  }
  
}

void loop() {
Serial.println(digitalRead(CapteurG));
Serial.println(digitalRead(CapteurD));
if(0 == digitalRead(CapteurG)) 
{
  driveMotDroit(170);
  driveMotGauche(0);
  delay(250);
  driveMotDroit(0);
  driveMotGauche(0);
}

if(0 == digitalRead(CapteurD))
{
  driveMotDroit(0);
  driveMotGauche(170);
  delay(500);
  driveMotDroit(0);
  driveMotGauche(0);
}
if(0 != digitalRead(CapteurD) && 0 != digitalRead(CapteurG))
{
  driveMotDroit(170);
  driveMotGauche(170);
  delay(250);
  driveMotDroit(0);
  driveMotGauche(0);
}


  // put your main code here, to run repeatedly:
}
