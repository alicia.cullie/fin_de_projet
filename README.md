 ## Robot suiveur de ligne
 
 ### SOMMAIRE
 
 
 ### Présentation du projet:
 
 Le robot suiveur de ligne est une base roulante, qui detecte une ligne de couleur , noire ou blanche (j utiliserai la ligne noire) pour pouvoir se deplacer. la base roulante se compose principalement de 2 roue moteurs et d un circuit électronique qui est la carte arduino.
 
 Pour que le robot puisse se déplacer sur la ligne noire, j'ai équiper mon robot de deux capteurs CNY70. Le CNY70 est un capteur optique réflectif qui inclut un émetteur infrarouge et un phototransistor la ligne noire étant associé à zero car la lumière est absorbé par le capteur et non réfléchi. Par conséquent  le blanc est associé à la valeur 1, si un de mes capteurs situé sur les deux cotés de mon robot détecte la ligne noire, j'adapte la situation pour commander mes moteurs.
 En conclusion:
 Le capteur droit détecte une ligne noire il dit au moteur droit de fonctionner et du coup d'avancer.
 Le capteur gauche détecte une ligne noire il dit au moteur gauche de fonctionner.





A effectuer:
- Déterminer un cas d'arret du robot.
- Optimiser le robot.
- Ajout d'un support batterie
- Fixation des éléments aux chassis

Amélioration:
 - Chassis plus grand

 
 
 
 
 